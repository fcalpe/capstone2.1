console.log(window.location.search);

// instantiate a URLSearchParams object so we can execute methods to access the parameters
let params = new URLSearchParams(window.location.search);

console.log(params);
console.log(params.has('courseId'));
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

// let viewEnrollees = document.querySelector("viewEnrollees")

let adminUser = localStorage.getItem("isAdmin");

console.log(adminUser);

let token = localStorage.getItem('token');


fetch(`https://cryptic-fortress-89487.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data);


	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;

	if(adminUser === "false" || !adminUser) {

		enrollContainer.innerHTML =
		`
			<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>
		`

		document.querySelector("#enrollButton").addEventListener("click", () => {

			fetch('https://cryptic-fortress-89487.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`

				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data);

				if(data === true){

					alert("Thank you for enrolling! See you in class!");
					window.location.replace("courses.html")
					
				} else {
					alert("something went wrong")
				}
			})
		})

	} else {

		viewEnrollees.innerHTML =
		`
			<div>
				<section class="jumbotron">
					<h3>Enrollees</h3>
					<table class="table">
						<thead>
							<tr>
								<th>First Name</th>
								<th>Last Name</th>
							</tr>
						</thead>
						<tbody id="enrolleesData">
							
						</tbody>
					</table>
				</section>
			</div>
		`

		enrolleesDetails = document.querySelector("#enrolleesData")

		data.enrollees.map(enrollment => {

			console.log(enrollment);

			fetch(`https://cryptic-fortress-89487.herokuapp.com/api/users/details/${enrollment.userId}`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				enrolleesDetails.innerHTML += // '+=' since we use map, the function is applied/looped to all iterations. If just '=' it will overlap the first iteration, resulting in 1 output

				`
					<tr>
						<td>${data.firstName}</td>
						<td>${data.lastName}</td>
					</tr>
				`
			})
		})
	}
})
